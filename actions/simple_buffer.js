/* 
 * actions/simple_buffer.js 
 *   use a simple buffer to avoid long delay in retention trimming.
 */

var simple_buffer_enabled = true /* true to use simple buffer */
var simple_buffer_streams = new Object()
var simple_buffer_streams_get_count = new Object()

var simple_buffer_seqn_min = 101 /* valid seqn incl */
var simple_buffer_seqn_max = 0x3fffffff /* valid seqn incl. 1,073,741,823 */
var simple_buffer_seqn_len = 10

/* some special small numbers to indicate special conditions */
var simple_buffer_millisBehind_error = 11
var simple_buffer_millisBehind_empty = 1
var simple_buffer_millisBehind_tail  = 2
var simple_buffer_millisBehind_min_valid  = 20
var simple_buffer_millisBehind_min_accurate  = 30

var simple_buffer_save_length = 192 /* max num of records, <(max-min)/8 */

module.exports.get_enabled = function() {
    return simple_buffer_enabled
}

module.exports.get_milliis_special = function() {
    return {
        'error': simple_buffer_millisBehind_error,
        'empty': simple_buffer_millisBehind_empty,
        'tail': simple_buffer_millisBehind_tail, 
        'min_valid': simple_buffer_millisBehind_min_valid,
        'min_accurate': simple_buffer_millisBehind_min_accurate
    }
}

module.exports.get_seqn_len = function() {
    return simple_buffer_seqn_len
}

module.exports.inc_seqn = function(seqn) {
  retn = seqn + 1
  if (retn > simple_buffer_seqn_max) {
    retn = simple_buffer_seqn_min
  }
  return retn
}

function has_stream(stream_name) {
    return simple_buffer_streams.hasOwnProperty(stream_name)
}

module.exports.put_record = function (stream_name, data) {
    var seqn
    data['ApproximateArrivalTimestamp'] = Date.now()/1000
    if ( has_stream(stream_name) ) {
        slen = simple_buffer_streams[stream_name].length
        lastSeqn = simple_buffer_streams[stream_name][slen-1]['SequenceNumber']
        seqn = lastSeqn + 1
        if (seqn > simple_buffer_seqn_max) {
            seqn = simple_buffer_seqn_min
            simple_buffer_streams[stream_name] = Array(0) /* remove all */
        }
        data['SequenceNumber'] = seqn
    } else { 
        /* new stream */
        seqn = simple_buffer_seqn_min
        simple_buffer_streams[stream_name] = Array(0)
        data['SequenceNumber'] = seqn
        simple_buffer_streams_get_count[stream_name] = 0
    }
    simple_buffer_streams[stream_name].push(data)
    var max_len = simple_buffer_save_length
    if ( simple_buffer_streams[stream_name].length >= max_len ) {
        simple_buffer_streams[stream_name].shift() /* remove oldest */
    }
    return seqn
}

module.exports.get_record = function (stream_name, seqn) {
  if ( has_stream(stream_name) ) {
      slen = simple_buffer_streams[stream_name].length
      if ( slen < 1 ) {
          return {}
      }
      firstSeqn = simple_buffer_streams[stream_name][0]['SequenceNumber']
      lastIdx = simple_buffer_streams[stream_name].length-1
      lastSeqn = simple_buffer_streams[stream_name][lastIdx]['SequenceNumber']
      if (lastSeqn >= firstSeqn) {
          if ( lastSeqn > simple_buffer_seqn_max/4*3 ) {
              /* wrap above the top when the buffer is working at 3/4+ zone, and 
               * the requested seqn is in 1/4- zone. 
               * the purpose is to be able to detect a request beyond tail. 
               */
              if ( seqn < simple_buffer_seqn_max/4 && 
                              seqn >= simple_buffer_seqn_min) {
                  seqn -= simple_buffer_seqn_min
                  seqn += simple_buffer_seqn_max + 1
              }
          }
          if ( seqn < firstSeqn ) {
              data = simple_buffer_streams[stream_name][0]
              data['simple_buffer_seqn_head'] = firstSeqn
              data['simple_buffer_seqn_tail'] = lastSeqn
              data['simple_buffer_index_ok'] = 0
              simple_buffer_streams_get_count[stream_name] ++
              data['simple_buffer_get_count'] = simple_buffer_streams_get_count[stream_name] 
              return data
          } else if ( seqn > lastSeqn ) {
              data = {}
              data['simple_buffer_seqn_head'] = firstSeqn
              data['simple_buffer_seqn_tail'] = lastSeqn
              data['simple_buffer_index_ok'] = lastIdx
              data['simple_buffer_index_beyond_tail'] = lastIdx
              simple_buffer_streams_get_count[stream_name] ++
              data['simple_buffer_get_count'] = simple_buffer_streams_get_count[stream_name] 
              return data
          } else {
              idx = seqn - firstSeqn
              if (idx < slen) {
                  data = simple_buffer_streams[stream_name][idx]
                  data['simple_buffer_seqn_head'] = firstSeqn
                  data['simple_buffer_seqn_tail'] = lastSeqn
                  data['simple_buffer_index_ok'] = idx
                  simple_buffer_streams_get_count[stream_name] ++
                  data['simple_buffer_get_count'] = simple_buffer_streams_get_count[stream_name] 
                      return data
              } else {
                  return { Error: "not implemented 1"}
              }
          }
      } else {
          return { Error: "not implemented 2"}
      }
  } else {
      return {}
  }
}

module.exports.get_seq_at_stamp = function (stream_name, stamp) {
  if ( has_stream(stream_name) ) {
      slen = simple_buffer_streams[stream_name].length
      if ( slen < 1 ) {
          return {}
      }
      var ret_idx = slen - 1 /* worst case, give the first record in buffer */
      for (i=slen-1; i>= 0; i--) {
          t = simple_buffer_streams[stream_name][i]['ApproximateArrivalTimestamp']
          if (stamp > t) {
              break
          }
          ret_idx = i
      }
      return { 'simple_buffer_seqn': 
               simple_buffer_streams[stream_name][ret_idx]['SequenceNumber'] }
  } else {
      return {}
  }
}

/* special note: apply this patch to help debug

$ git diff ../index.js
diff --git a/index.js b/index.js
index da25bb8..37225a8 100644
--- a/index.js
+++ b/index.js
@@ -250,7 +250,27 @@ function httpHandler(store, req, res) {
       throw e
     }

+    console.log("action to call: " + action)
+    var simple_buffer_log = false
+    if (simple_buffer_log) {
+      if (action == 'putRecord') {
+        console.log(" action putRecord data " + data.toString())
+        for (x in data) {
+          console.log("    " + x.toString() + ' = ' + data[x])
+        }
+      }
+    }
     actions[action](store, data, function(err, data) {
+      if (action == 'getRecords') {
+        if ( data == undefined ) {
+          console.log(" action getRecords data undefined")
+        } else {
+          console.log(" action getRecords data " + data.toString())
+          for (x in data) {
+            console.log("    " + x.toString() + ' = ' + data[x])
+          }
+        }
+      }
       if (err && err.statusCode) return sendResponse(req, res, err.body, err.statusCode)
       if (err) throw err
       sendResponse(req, res, data)

*/

