var crypto = require('crypto'),
    once = require('once'),
    db = require('../db')

module.exports = function getRecords(store, data, cb) {

  /* a variation of data store in a simple buffer */
  var simple_buffer = false
  var simple_buffer_ex = require('./simple_buffer.js')
  if ( simple_buffer_ex.get_enabled() ) {
    simple_buffer = true
  } else {
    console.log("    get simple not enabled  ")
  }

  var metaDb = store.metaDb, shardIx, shardId, iteratorTime, streamName, seqNo, seqObj, pieces,
    buffer = Buffer.from(data.ShardIterator, 'base64'), now = Date.now(),
    decipher = crypto.createDecipheriv('aes-256-cbc', db.ITERATOR_PWD_KEY, db.ITERATOR_PWD_IV)

  //if (buffer.length < 152 || buffer.length > 280 || buffer.toString('base64') != data.ShardIterator)
  buffer_len_min = 152 // original lower bound when seqn is 56 bytes
  if ( simple_buffer ) {
    buffer_len_min = 104
  }
  if (buffer.length < buffer_len_min || buffer.length > 280 || buffer.toString('base64') != data.ShardIterator)
    return cb(invalidShardIterator())

  if (buffer.slice(0, 8).toString('hex') != '0000000000000001')
    return cb(invalidShardIterator())

  try {
    pieces = Buffer.concat([decipher.update(buffer.slice(8)), decipher.final()]).toString('utf8').split('/')
  } catch (e) {
    return cb(invalidShardIterator())
  }

  if (pieces.length != 5)
    return cb(invalidShardIterator())

  iteratorTime = +pieces[0]
  streamName = pieces[1]
  shardId = pieces[2]
  seqNo = pieces[3]

  shardIx = parseInt(shardId.split('-')[1])
  if (!/^shardId-[\d]{12}$/.test(shardId) || !(shardIx >= 0 && shardIx < 2147483648))
    return cb(invalidShardIterator())

  if (!(iteratorTime > 0 && iteratorTime <= now))
    return cb(invalidShardIterator())

  if (!/[a-zA-Z0-9_.-]+/.test(streamName) || !streamName.length || streamName.length > 128)
    return cb(invalidShardIterator())

  if ((now - iteratorTime) > 300000) {
    return cb(db.clientError('ExpiredIteratorException',
      'Iterator expired. The iterator was created at time ' + toAmzUtcString(iteratorTime) +
      ' while right now it is ' + toAmzUtcString(now) + ' which is further in the future than the ' +
      'tolerated delay of 300000 milliseconds.'))
  }

  try {
    seqObj = db.parseSequence(seqNo)
  } catch (e) {
    return cb(invalidShardIterator())
  }

  store.getStream(streamName, function(err, stream) {
    if (err) {
      if (err.name == 'NotFoundError' && err.body) {
        err.body.message = 'Shard ' + shardId + ' in stream ' + streamName +
          ' under account ' + metaDb.awsAccountId + ' does not exist'
      }
      return cb(err)
    }
    if (shardIx >= stream.Shards.length) {
      return cb(db.clientError('ResourceNotFoundException',
        'Shard ' + shardId + ' in stream ' + streamName +
        ' under account ' + metaDb.awsAccountId + ' does not exist'))
    }

    cb = once(cb)

    var streamDb = store.getStreamDb(streamName), cutoffTime = now - (stream.RetentionPeriodHours * 8 * 1000),
      keysToDelete = [], lastItem, opts

    opts = {
      gte: db.shardIxToHex(shardIx) + '/' + seqNo,
      lt: db.shardIxToHex(shardIx + 1),
    }

    if ( simple_buffer ) {
      var nextSeq, seqType = typeof seqNo
      var slen = seqNo.length /* kinesis uses a 56 bytes string */
      var max_len = simple_buffer_ex.get_seqn_len()
      if ( seqType == "string" ) {
        if ( slen > max_len ) { /* simple buffer format */
          //console.log("  seq 1 case 1")
          nextSeq = 1 /* will get next valid record above 1 */
        } else {
          //console.log("  seq " + seqNo + " case 2 ")
          nextSeq = parseInt(seqNo)
        }
      } else {
        //console.log("    seq 0 case 3 ")
        nextSeq = 0
      }
      var data = simple_buffer_ex.get_record(streamName, nextSeq)
      var ret_rec = Array(0)
      var millis = simple_buffer_ex.get_milliis_special()
      if ( data.hasOwnProperty('simple_buffer_index_beyond_tail') ) {
          millisBehind = millis.tail /* 2: a special indicator */
          nextSeq = nextSeq /* no change */
      } else if ( data.hasOwnProperty('simple_buffer_index_ok') ) {
          var arvtime = Math.floor(data['ApproximateArrivalTimestamp'] * 1000)
          if ( now >= arvtime ) {
              millisBehind = now - arvtime
              if ( millisBehind < millis.min_accurate ) {
                  if ( millisBehind == 0 ) {
                      millisBehind = millis.min_valid + 5
                  } else if ( millisBehind < 0 ) {
                      millisBehind = millis.min_valid
                  } else if ( millisBehind < millis.min_valid ) {
                      millisBehind = millis.min_valid + 6
                  } else {
                      millisBehind = millis.min_valid + 7
                  }
              }
          } else {
              millisBehind = millis.error /* 11: a special error indicator */
          }
          nextSeq = simple_buffer_ex.inc_seqn(data['SequenceNumber'])
          ret_rec.push(data)
          get_count = data['simple_buffer_get_count']
          if ( (get_count % 60) == 1) {
              logmsg = "    stream " + streamName 
              logmsg += " current seq " + data['simple_buffer_index_ok']
              logmsg += " head " + data['simple_buffer_seqn_head']
              logmsg += " tail " + data['simple_buffer_seqn_tail']
              logmsg += " get count " + get_count
              console.log(logmsg)
          }
      } else {
          millisBehind = millis.empty /* 1: a special indicator */
          nextSeq = nextSeq /* no change */
      }
      nextShardIterator = db.createShardIterator(streamName, shardId, nextSeq)

      /* example element in Records arrary:
       *     ApproximateArrivalTimestamp: 1588286456.39
       *     Data: "YWJjZA=="
       *     PartitionKey: "partitionkey"
       *     SequenceNumber: "4960657629314787944412189313192..."
       */
      return cb(null, {
        MillisBehindLatest: millisBehind,
        NextShardIterator: nextShardIterator,
        Records: ret_rec
      })
    }

    db.lazy(streamDb.createReadStream(opts), cb)
      .take(data.Limit || 10000)
      .map(function(item) {
        lastItem = item.value
        lastItem.SequenceNumber = item.key.split('/')[1]
        lastItem._seqObj = db.parseSequence(lastItem.SequenceNumber)
        lastItem._tooOld = lastItem._seqObj.seqTime < cutoffTime
        if (lastItem._tooOld) keysToDelete.push(item.key)
        return lastItem
      })
      .filter(function(item) { return !item._tooOld })
      .join(function(items) {
        var defaultTime = now
        if (seqObj.seqTime > defaultTime) {
          defaultTime = seqObj.seqTime
        }
        var nextSeq = db.incrementSequence(lastItem ? lastItem._seqObj : seqObj, lastItem ? null : defaultTime),
          nextShardIterator = db.createShardIterator(streamName, shardId, nextSeq),
          millisBehind = 0

        if (!items.length && stream.Shards[shardIx].SequenceNumberRange.EndingSequenceNumber) {
          var endSeqObj = db.parseSequence(stream.Shards[shardIx].SequenceNumberRange.EndingSequenceNumber)
          if (seqObj.seqTime >= endSeqObj.seqTime) {
            nextShardIterator = undefined
            millisBehind = Math.max(0, now - endSeqObj.seqTime)
          }
        }
        if (millisBehind == 0) millisBehind = Math.max(0, now - seqObj.seqTime)

        cb(null, {
          MillisBehindLatest: millisBehind,
          NextShardIterator: nextShardIterator,
          Records: items.map(function(item) {
            delete item._seqObj
            delete item._tooOld
            return item
          }),
        })

        if (keysToDelete.length) {
          // Do this async
          streamDb.batch(keysToDelete.map(function(key) { return {type: 'del', key: key} }), function(err) {
            if (err && !/Database is not open/.test(err)) console.error(err.stack || err)
          })
        }
      })
  })
}

function invalidShardIterator() {
  return db.clientError('InvalidArgumentException', 'Invalid ShardIterator.')
}

// Thu Jan 22 01:22:02 UTC 2015
function toAmzUtcString(date) {
  var pieces = new Date(date).toUTCString().match(/^(.+), (.+) (.+) (.+) (.+) GMT$/)
  /* pieces[0]: "Thu, 30 Apr 2020 20:41:52 GMT"
   * picees[1]: "Thu"
   * pieces[2]: "30"
   * pieces[3]: "Apr"
   * pieces[4]: "2020"
   * pieces[5]: "20:41:52"
   */
  return [pieces[1], pieces[3], pieces[2], pieces[5], 'UTC', pieces[4]].join(' ')
}
